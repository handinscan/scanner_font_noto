#!/bin/bash

START_PWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

ARTIFACT_REPO_ROOT_DIR=$1
RESULT_PATH=${START_PWD}/result
VERSION_JSON=./version.json

#cp to result path
rm -rf ${RESULT_PATH}
mkdir -p ${RESULT_PATH}
rsync -arv ${START_PWD}/* ${RESULT_PATH} --exclude copy_to_artifact_repo.sh --exclude result/ --exclude Jenkinsfile 

# add git commit hash
GIT_HASH_L=$(git rev-parse --verify HEAD)
echo $(jq '.git.commit='\"$GIT_HASH_L\" ${VERSION_JSON}) > ${RESULT_PATH}/version.json


TOOL_CMAKE_VERSION=$(jq ".build_tool.version" $VERSION_JSON | tr -d '"')
TOOL_CMAKE_ROOT=${ARTIFACT_REPO_ROOT_DIR}/host/tool_cmake/${TOOL_CMAKE_VERSION}
ARTIFACT_REPO_STORE_SH=${TOOL_CMAKE_ROOT}/artifact_repo/store.sh
TOOLCHAINS_JSON=${TOOL_CMAKE_ROOT}/toolchain/toolchains.json

$ARTIFACT_REPO_STORE_SH store $(jq ".alias.latest.arm" ${TOOLCHAINS_JSON} |  tr -d '"') $(jq ".version" ${VERSION_JSON} | tr -d '"') $(jq ".artifact_name" ${VERSION_JSON} | tr -d '"') ${RESULT_PATH}/. run $ARTIFACT_REPO_ROOT_DIR

